# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: 1.3~rc2-1-ga15645d\n"
"PO-Revision-Date: 2015-02-23 11:35+0200\n"
"Last-Translator: Michal Čihař <michal@cihar.com>\n"
"Language-Team: Spanish <https://hosted.weblate.org/projects/noosfero/plugin-so"
"lr/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.3-dev\n"

msgid "Older than one year"
msgstr "Más de un año"

msgid "Published date"
msgstr "Fecha de publicación"

msgid "In the last day"
msgstr "Ayer"

msgid "In the last week"
msgstr "En la semana pasada"

msgid "In the last month"
msgstr "En el mes pasado"

msgid "In the last year"
msgstr "En el año pasado"

msgid "Related products"
msgstr "Productos relacionados"

msgid " cert. "
msgstr " cert. "

msgid "Situation"
msgstr "Situación"

#, fuzzy
msgid "facets|Enabled"
msgstr "Habilitado"

# habilitado o permitido? estaba o está?
#, fuzzy
msgid "facets|Not enabled"
msgstr "No habilitado"

msgid "Uses Solr as search engine."
msgstr ""

msgid "Relevance"
msgstr "Relevante"

msgid "Name"
msgstr "Nombre"

msgid "Closest to me"
msgstr "Más cercano a mí"

msgid "Sort results by "
msgstr "Ordenar resultados por"

msgid "%s products offers found"
msgstr "%s ofertas de productos encontradas"

msgid "%s articles found"
msgstr "%s artículos encontrados"

msgid "%s events found"
msgstr "%s eventos encontrados"

msgid "%s people found"
msgstr "%s personas encontrados"

msgid "%s enterprises found"
msgstr "%s empresas encontradas"

msgid "%s communities found"
msgstr "%s comunidades encontradas"

msgid "Type in an option"
msgstr "Teclee una opción"

msgid "No filter available"
msgstr "Sin filtro disponible"

msgid "Showing page %s of %s"
msgstr "Mostrando página %s de %s"
